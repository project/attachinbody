<?php

namespace Drupal\attachinbody;

/**
 * Storage for libraries included in the page body.
 */
class InBodyLibraries {

  /**
   * An array of libraries.
   *
   * @var array
   */
  private $libraries = [];

  /**
   * Add a library.
   *
   * @param string $library
   *   The library name.
   */
  public function addLibrary($library) {
    $this->libraries[] = $library;
  }

  /**
   * Get the array of libraries.
   *
   * @return array
   *   An array of library names.
   */
  public function getLibraries() {
    return $this->libraries;
  }

}
