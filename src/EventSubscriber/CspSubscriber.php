<?php

namespace Drupal\attachinbody\EventSubscriber;

use Drupal\csp\Csp;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Act on CSP events.
 */
class CspSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    if (!class_exists(CspEvents::class)) {
      return [];
    }

    $events[CspEvents::POLICY_ALTER] = ['onCspPolicyAlter'];
    return $events;
  }

  /**
   * Add hashes to the provided policy.
   *
   * Hashes are only added if the corresponding directive is enabled in module
   * configuration.
   *
   * Only add hashes if the policy does not already include 'unsafe-inline',
   * otherwise non-hashed inline JS may be unexpectedly blocked.
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $event
   *   The Policy Alter Event.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $event) {
    $policy = $event->getPolicy();

    $hash = "'" . Csp::calculateHash(' ') . "'";
    static::fallbackAwareAppendIfEnabled($policy, 'script-src', [$hash]);
    static::fallbackAwareAppendIfEnabled($policy, 'script-src-elem', [$hash]);
  }

  /**
   * Append to a directive if it or a fallback directive is enabled.
   *
   * If the specified directive is not enabled but one of its fallback
   * directives is, it will be initialized with the same value as the fallback
   * before appending the new value.
   *
   * If none of the specified directive's fallbacks are enabled, the directive
   * will not be enabled.
   *
   * @param \Drupal\csp\Csp $policy
   *   The policy to alter.
   * @param string $directive
   *   The directive name.
   * @param array $value
   *   The directive value.
   *
   * @see Csp::fallbackAwareAppendIfEnabled()
   */
  protected static function fallbackAwareAppendIfEnabled(Csp $policy, $directive, array $value) {
    if ($policy->hasDirective($directive)) {
      if (!in_array(Csp::POLICY_UNSAFE_INLINE, $policy->getDirective($directive))) {
        $policy->appendDirective($directive, $value);
      }
      return;
    }

    // Duplicate the closest enabled fallback directive.
    foreach ($policy::getDirectiveFallbackList($directive) as $fallback) {
      if ($policy->hasDirective($fallback)) {
        $fallbackValue = $policy->getDirective($fallback);
        // Don't make any modifications if closest enabled fallback uses
        // 'unsafe-inline'.
        if (in_array(Csp::POLICY_UNSAFE_INLINE, $fallbackValue)) {
          return;
        }
        $policy->setDirective($directive, $fallbackValue);
        $policy->appendDirective($directive, $value);
        return;
      }
    }
  }

}
