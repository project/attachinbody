<?php

namespace Drupal\attachinbody;

use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Decorate the core asset resolver to remove in-body libraries.
 */
class AssetResolverDecorator implements AssetResolverInterface {

  /**
   * A map of processed asset objects.
   *
   * @var \SplObjectStorage
   */
  private \SplObjectStorage $processedAssets;

  /**
   * AssetResolverDecorator constructor.
   *
   * @param \Drupal\Core\Asset\AssetResolverInterface $decorated
   *   The Asset Resolver service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The Library Discovery service.
   * @param \Drupal\attachinbody\InBodyLibraries $inBodyLibraries
   *   The In-Body Libraries service.
   */
  public function __construct(
    private AssetResolverInterface $decorated,
    private LibraryDiscoveryInterface $libraryDiscovery,
    private InBodyLibraries $inBodyLibraries,
  ) {
    $this->processedAssets = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function getCssAssets(AttachedAssetsInterface $assets, $optimize, LanguageInterface $language = NULL) {
    $this->updateAssets($assets);

    return $this->decorated->getCssAssets($assets, $optimize, $language);
  }

  /**
   * {@inheritdoc}
   */
  public function getJsAssets(AttachedAssetsInterface $assets, $optimize, LanguageInterface $language = NULL) {
    $this->updateAssets($assets);

    return $this->decorated->getJsAssets($assets, $optimize, $language);
  }

  /**
   * Update the AttachedAssets for libraries included in the page body.
   *
   * @param \Drupal\Core\Asset\AttachedAssetsInterface $assets
   *   The attached assets for the page.
   */
  private function updateAssets(AttachedAssetsInterface $assets): void {
    if ($this->processedAssets->contains($assets)) {
      return;
    }
    $this->processedAssets->attach($assets);

    // Replace the in-body library with its dependencies.
    $libraries = $assets->getLibraries();
    foreach ($this->inBodyLibraries->getLibraries() as $inBodyLibrary) {
      [$extension, $name] = explode('/', $inBodyLibrary, 2);
      $definition = $this->libraryDiscovery->getLibraryByName($extension, $name);
      $libraries = array_merge($libraries, $definition['dependencies'] ?? []);

      $libraryIndex = array_search($inBodyLibrary, $libraries);
      unset($libraries[$libraryIndex]);
    }
    $assets->setLibraries($libraries);
  }

}
