<?php

namespace Drupal\attachinbody;

use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Register Twig functions.
 */
class TwigExtension extends AbstractExtension {

  /**
   * The site performance configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $performanceConfig;

  /**
   * The storage for libraries included in the page body.
   *
   * @var \Drupal\attachinbody\InBodyLibraries
   */
  private $inBodyLibraries = [];

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The Library Discover service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  private $libraryDiscovery;

  /**
   * The Asset Resolver service.
   *
   * @var \Drupal\Core\Asset\AssetResolverInterface
   */
  private $assetResolver;

  /**
   * The CSS Renderer service.
   *
   * @var \Drupal\Core\Asset\AssetCollectionRendererInterface
   */
  private $cssRenderer;

  /**
   * The JS renderer service.
   *
   * @var \Drupal\Core\Asset\AssetCollectionRendererInterface
   */
  private $jsRenderer;

  /**
   * Initialize the AttachInBody Twig Extension.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The Library Discovery Service.
   * @param \Drupal\attachinbody\InBodyLibraries $inBodyLibraries
   *   The storage for libraries included in the page body.
   * @param \Drupal\Core\Asset\AssetResolverInterface $assetResolver
   *   The Asset Resolver service.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $cssRenderer
   *   The CSS Renderer service.
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $jsRenderer
   *   The JS Renderer service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer, LibraryDiscoveryInterface $libraryDiscovery, InBodyLibraries $inBodyLibraries, AssetResolverInterface $assetResolver, AssetCollectionRendererInterface $cssRenderer, AssetCollectionRendererInterface $jsRenderer) {
    $this->performanceConfig = $config_factory->get('system.performance');
    $this->renderer = $renderer;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->inBodyLibraries = $inBodyLibraries;
    $this->assetResolver = $assetResolver;
    $this->cssRenderer = $cssRenderer;
    $this->jsRenderer = $jsRenderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('attach_library_inbody', [$this, 'attachInBody']),
    ];
  }

  /**
   * Attaches an asset library to the template, and hence to the response.
   *
   * Allows Twig templates to attach asset libraries using:
   * @code
   * {{ attach_library_inbody('extension/library_name') }}
   * @endcode
   *
   * @param string $library
   *   An asset library.
   *
   * @return string
   *   Rendered HTML for library assets.
   */
  public function attachInBody($library) {
    assert(is_string($library), 'Argument must be a string.');

    $render = ['#attached' => ['library' => [$library]]];
    $assets = AttachedAssets::createFromRenderArray($render);

    // Exclude any dependencies from being rendered here.
    [$extension, $name] = explode('/', $library, 2);
    $definition = $this->libraryDiscovery->getLibraryByName($extension, $name);
    $assets->setAlreadyLoadedLibraries($definition['dependencies'] ?? []);

    $optimize_css = !defined('MAINTENANCE_MODE') && $this->performanceConfig->get('css.preprocess');
    $cssAssets = $this->assetResolver->getCssAssets($assets, $optimize_css);
    $render = array_merge($render, $this->cssRenderer->render($cssAssets));

    $optimize_js = !defined('MAINTENANCE_MODE') && $this->performanceConfig->get('js.preprocess');
    $jsAssets = $this->assetResolver->getJsAssets($assets, $optimize_js);
    $render = array_merge($render, $this->jsRenderer->render($jsAssets[0]));
    $render = array_merge($render, $this->jsRenderer->render($jsAssets[1]));

    // Firefox doesn't halt rendering when encountering in-body CSS, which can
    // result in FOUC for later content, but halting can be forced by a
    // non-empty, non-async JS.
    // @see https://jakearchibald.com/2016/link-in-body/#firefixing
    $firefix = TRUE;
    foreach ($jsAssets as $jsAssetGroup) {
      foreach ($jsAssetGroup as $jsAsset) {
        if (empty($jsAsset['attributes']['async'])) {
          $firefix = FALSE;
          break 2;
        }
      }
    }
    if ($firefix) {
      $render['firefix'] = [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#value' => ' ',
        '#weight' => PHP_INT_MAX,
      ];
    }

    // Library needs to be stored after any calls to Asset Resolver service.
    $this->inBodyLibraries->addLibrary($library);

    return $this->renderer->render($render);
  }

}
